#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */

struct node {
	string data;
	node *next = NULL;
	node *prev = NULL;
};
bool inequal(bool ign, const string string1, const string string2, int ineq){
	if (ign){
		if (ineq == 0){
			return strcasecmp(string1.c_str(), string2.c_str()) < 0; 
		}
		else if(ineq == 1){
			return strcasecmp(string1.c_str(), string2.c_str()) == 0; 
		}
		else if(ineq == 2){
			return strcasecmp(string1.c_str(), string2.c_str()) > 0 ; 
		}
		else if(ineq == 3){
			return strcasecmp(string1.c_str(), string2.c_str()) <= 0 ; 
		}
		else{
			return strcasecmp(string1.c_str(), string2.c_str()) >= 0 ; 
		}
	}
	else{
		if (ineq == 0){
			return string1 < string2;	
		}
		else if(ineq == 1){
			return string1 == string2;
		}
		else if(ineq == 2){
			return string1 > string2;
		}
		else if(ineq == 3){
			return string1 <= string2;
		}
		else{
			return string1 >= string2;
		}
	}
}

void insert(struct node** head, struct node** tail, struct node* n, bool ign, bool uniq){
	node* current = *head;
	if (*head == NULL){
		*head = n;
	}
	else if(inequal(ign,(*head)->data,n->data,4)){
		if (!uniq || !inequal(ign,current->data, n->data,1)){
			n->next = *head;
			(*head)->prev = n;
			*head = n;
		}
	}
	else{
		while(current->next != NULL && inequal(ign,current->next->data, n->data,0)){
			current = current->next;
		}
		if (!uniq || !inequal(ign,current->data, n->data,1)){
			n->next = current->next;
			n->prev = current;
			if (current->next != NULL){
				current->next->prev = n;
			}
			current->next = n;
		}
	}
	if(n->next == NULL){
		*tail = n;
	}
}

void output (bool reverse, node** head, node** tail){
	if (reverse){
		node* current = *tail;
		while (current != NULL){
			cout << current->data << "\n";
			current = current->prev;
		}
	}
	else{
		node* current = *head;
		while (current != NULL){
			cout << current->data << "\n";
			current = current->next;
		}
	}
}

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	node* first = NULL;
	node* last = NULL;
	string x;
	while (std::getline(cin,x)){
		node* n = new node;
		n->data = x;
		insert(&first,&last,n,ignorecase,unique);
	}
	output(descending,&first,&last);
	/*
	if (ignorecase && unique){
		set<string,igncaseComp> lines;
		string n;
		while (std::getline(cin,n)){
			lines.insert(n);
		}
		if (descending){
			set<string,igncaseComp>::reverse_iterator itr;
			for (itr = lines.rbegin(); itr != lines.rend(); itr++){
				cout << *itr << "\n";
			}
		}
		else{	
			set<string,igncaseComp>::iterator itr;
			for (itr = lines.begin(); itr != lines.end(); itr++){
				cout << *itr << "\n";
			}
		}
	}
	else if(ignorecase){
		multiset<string,igncaseComp> lines;
		string n;
		while (std::getline(cin,n)){
			lines.insert(n);
		}
		if (descending){
			multiset<string,igncaseComp>::reverse_iterator itr;
			for (itr = lines.rbegin(); itr != lines.rend(); itr++){
				cout << *itr << "\n";
			}
		}
		else{	
			multiset<string,igncaseComp>::iterator itr;
			for (itr = lines.begin(); itr != lines.end(); itr++){
				cout << *itr << "\n";
			}
		}
	}
	else if (unique){
		set<string> lines;
		string n;
		while (std::getline(cin,n)){
			lines.insert(n);
		}
		if (descending){
			set<string>::reverse_iterator itr;
			for (itr = lines.rbegin(); itr != lines.rend(); itr++){
				cout << *itr << "\n";
			}
		}
		else{	
			set<string>::iterator itr;
			for (itr = lines.begin(); itr != lines.end(); itr++){
				cout << *itr << "\n";
			}
		}
	}
	else{
		multiset<string> lines;
		string n;
		while (std::getline(cin,n)){
			lines.insert(n);
		}
		if (descending){
			multiset<string>::reverse_iterator itr;
			for (itr = lines.rbegin(); itr != lines.rend(); itr++){
				cout << *itr << "\n";
			}
		}
		else{	
			multiset<string>::iterator itr;
			for (itr = lines.begin(); itr != lines.end(); itr++){
				cout << *itr << "\n";
			}
		}
	}	*/
	return 0;
}

