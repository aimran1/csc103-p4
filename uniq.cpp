/*Refrences: cpluplus.com and group */
#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include<iomanip> //to use setw//
using std::setw; //sets field with//

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";


int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	string prev,curr;
	size_t linecount(0);
	bool inp = true;
	while(!feof(stdin)){ //reads file in a way//
		getline(cin,prev);
		if(inp){
			curr=prev; //since inp is true curr becomes equal to prev//
			inp=false; //and inp now becoms false//
		}
		if(prev == curr)
			linecount++; //-c//
		else if(prev != curr){
			if(showcount==1 && dupsonly==0 && uniqonly==0)
				cout<< setw(7)<<linecount<<" "<<curr<<endl;//-c//
			else if(showcount==1 && dupsonly==1 && uniqonly==0 && linecount>1)
				cout<< setw(7)<<linecount<<" "<<curr<<endl;//-c//
			else if(showcount==1 && dupsonly==0 && uniqonly==1 && linecount==1)
				cout<< setw(7)<<linecount<<" "<<curr<<endl;//-c//
			else if(showcount==0 && dupsonly==1 && uniqonly==0 && linecount>1)
				cout<<curr<<endl;//-d//
			else if(showcount==0 && dupsonly==0 && uniqonly==1 && linecount==1)
				cout<<curr<<endl;//-u//
			else if(showcount==0 && dupsonly==0 && uniqonly==0)
				cout<<curr<<endl;
			else if(showcount==0 && dupsonly==1 && uniqonly==1){
			} /*since it is not possible to be both a duplicated line and a unique line?*/
			linecount=1;
			curr=prev;
		}
	}
	return 0;
}












/*
	string prev;
	string curr;
	unsigned long int count;
	string input;

	for(size_t i = 0; i < input.length(); i++)
	{
		prev=curr;
		while(std::getline(cin, curr))
			{
				if(curr == prev)
					{
						count++;
					}
					else
						{
							cout << prev;
							prev = curr;
						}
					}
					printf("%7lu ",count);
				}
*/				/*
	string prev;
	string curr;
	for (size_t i = 0; i < input.length(); i++) {
		curr += input '\n';
		if(input[i] == '\n') {
	*/

	/*string checker;
	int i = 0;
	while(input[i] != '\n'){
		checker.push_back(input[i]);
		i++;
	}
	i++;
	for(; i<(input.length), i++;)
	{
		string cl= "";
		while(input[i]!='\n'){
			cl.push_back(input[i]);
			i++;
		}
		if(cl!=checker){
			cout<<checker<<endl;
			checker=cl;
		}
	}*/




















